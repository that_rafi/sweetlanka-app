require("dotenv").config();
const express = require('express');
const viewEngine = require('./config/viewEngine');
const initWebRoutes = require('./routes/web');
var cookieParser = require('cookie-parser')


let app = express();
app.use(express.json());
app.use(cookieParser())

// init

viewEngine(app);
initWebRoutes(app);

let port = process.env.PORT || 3030;

app.listen(port, ()=>{
    console.log(`App is running on server ${port}`);
})