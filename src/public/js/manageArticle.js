var pathname = window.location.pathname.split('/');
    var BASE_URL = 'https://sweetlanka-server.herokuapp.com/api/article'
    // https://sweetlanka-server.herokuapp.com/api/article
     // http://localhost:3000/api/article

    var pageno = pathname[4];
    var dataArticle = [];
    // Upload Variable
    var listFile = [];
    var listKet = [];
    var logoData = '';
    var tempfiles = "";
    var tempImg = [];
    var tempDelImg = [];

    getData();

    $('#inputpic').on('change',function(e){
     //console.log(e.target.files[0]);
      var reader = new FileReader();
      logoData = e.target.files[0];
      reader.readAsDataURL(e.target.files[0]);
      reader.onload = function(event) {
        $('#imgbox').attr('style','background-image: url("'+event.target.result+'");background-size:cover')
        $('#imgplaceholder').html('<br/><br/><br/><br/><br/><br/><br/><br/>')
      }
    })

    $('#addinforow').on('click',function(e){
    e.preventDefault();
    var $div = $('tr[id^="listcount"]:last');
    if($div.length > 0 ){
        var num = parseInt( $div.prop("id").match(/\d+/g), 10 ) +1;
    }else{
        var num = 0;
    }

    var url = $('.dropzone_thumb').data('imgurl')
    var imgname = $('.dropzone_thumb').data('label')
    var keterangan = $('#inputKeterangan').val()
    if (num < 5 ){
      if( keterangan!= "" && url !=null){
        // data for form
        listFile.push(tempfiles)
        listKet.push(keterangan)
        var row = '  <tr id="listcount'+num+'">'+
                  '<td><img src='+url+' width="100px" height="100px"></img></td>'+
                  '<td>'+keterangan+'</td>'+
                  '<td><button id="minusinforow'+num+'" onclick="event.preventDefault(); minusinforow('+num+');" class="btn btn-warning"><i class="fa fa-trash"></i></a></td></tr>'
        $('#listinfo').append(row)
        $('.dropzone_thumb').remove()
        $('.dropzone_msg').show()
        $('.dropzone_msg').text('Drop more files')
        $('#inputKeterangan').val('')
      }else{
        alert('Please insert the detail of picture!')
      }
    }else{
      alert('You can not add more pictures!')
    }

  })

  function setImg(dropzone,files){
    if(files.type.startsWith("image/")){
    var thumbnail = $('.dropzone_thumb').data('label')
    dropzone.attr('class','dropzone')
    if(!thumbnail){
      $('.dropzone_msg').hide()
      var thumb = $('<div/>').addClass('dropzone_thumb');
      dropzone.append(thumb)
    }
    // data for form
    tempfiles = files
    $('.dropzone_thumb').attr('data-label',files.name) // set img name
      var reader = new FileReader();
      reader.readAsDataURL(files);
      reader.onload = function(event) {
            $('.dropzone_thumb').attr('style','background-image: url("'+event.target.result+'")')
            $('.dropzone_thumb').data('imgurl',event.target.result) // hold img url
      };
    }else{
      alert('Please input image only!')
    }
  }

  function minusinforow(id){
    $('#listcount'+id).remove()
    if(tempImg.length > 0 ){
      tempDelImg.push(tempImg[id])
      tempImg.splice(id,1)   
    }
    //console.log(tempImg);
    listFile.splice(id,1);
    listKet.splice(id,1)
  }

  jQuery(function($){
    $('.dropzone').on('click', function(){
        $('.dropzone_input')[0].click();
    });
  });

    $('.dropzone_input').on('change',function(e){
      $('.dropzone').attr('class','dropzone')
      setImg($('.dropzone'),e.target.files[0])
    })

    $('.dropzone').on('drop',function(e){
      e.preventDefault();
      $(this).attr('class','dropzone')
      setImg($(this),e.originalEvent.dataTransfer.files[0])
    })
    $('.dropzone').on('dragover',function(){
      $(this).attr('class','dropzone dragover')
      return false
    })
    $('.dropzone').on('dragleave',function(){
      $(this).attr('class','dropzone')
      return false
    })



    function getData(query=""){ 
      const params = new URLSearchParams(window.location.search);
      var keyword= params.get("search");
      if(query!= ""){
        keyword = query
      }
      var page = params.get("page");
      var limit = params.get("limit");
        $.ajax({
            type:'GET',
            // https://sweetlanka-server.herokuapp.com/api/article
            // http://localhost:3000/api/article
            url: BASE_URL+'?search='+keyword+'&page='+page+'&limit='+limit,
            dataType: 'json',
            success: function(data){
                console.log(data);
                var row = '';var loca_row='';
                // table
                for(var i=0;i<data.data.length;i++){
                    dataArticle[i] = {
                      _id : data.data[i]._id,
                      title : data.data[i].title,
                      author : data.data[i].author,
                      desc : data.data[i].desc,
                      images : data.data[i].images
                    }
                    row += '<tr>'+
                      '<td>'+data.data[i]._id+'</td>'+
                      '<td>'+data.data[i].title+'</td>'+
                      '<td>'+data.data[i].desc.slice(0, 20) + (data.data[i].desc.length > 20 ? "..." : "")+'</td>'+
                      '<td>'+data.data[i].author+'</td>'+
                      '<td>'+data.data[i].updatedAt+'</td>'+
                      '<td>'+'<button type="button" data-toggle="modal" onclick="getDataById('+i+')"  data-target="#modal-default" style="margin-right:5px" class="btn btn-outline-primary"><i class="fas fa-edit"></i></button>'+
                      '<button type="button" data-toggle="modal" data-target="#modal-danger-delete" data-id="'+i+'" id="btndel" class="btn btn-outline-danger"><i class="fas fa-trash"></i></button>'+
                      '</td>'+
                    '</tr>'
                }
                // Pagination
                var pageNumber = (Number(data.current) > 5 ? Number(data.current) - 4 : 1)
                var pageRow = '';
                if(data.current == 1){
                  pageRow += '<li class="page-item disabled" id="pageFirst"><a class="page-link">First</a></li>';
                }else{
                  pageRow += '<li class="page-item" id="pageFirst"><a class="page-link" href="'+window.location.origin+'/admin/article?page=1">First</a></li>';
                }
                if (pageNumber !== 1) {
                  pageRow += '<li class="page-item disabled" id="pageDotf"><a class="page-link">...</a></li>';
                }
                for (; pageNumber <= (Number(data.current) + 4) && pageNumber <= data.pages; pageNumber++) {
                  if (pageNumber == data.current) {
                    pageRow += '<li class="page-item active" ><a class="page-link" href="'+window.location.origin+'/admin/article?page='+pageNumber+'">'+pageNumber+'</a></li>';
                  }else{
                    pageRow += '<li class="page-item" ><a class="page-link" href="'+window.location.origin+'/admin/article?page='+pageNumber+'">'+pageNumber+'</a></li>';
                  }
                  if (pageNumber == Number(data.current) + 4 && pageNumber < data.pages) {
                   pageRow += '<li class="page-item disabled" id="pageDotl"><a class="page-link">...</a></li>';
                  }
                }
                
                if (data.current == data.pages) {
                  pageRow += '<li class="page-item disabled" id="pageLast"><a class="page-link">Last</a></li>';
                }else{
                  pageRow += '<li class="page-item" id="pageLast"><a class="page-link" href="'+window.location.origin+'/admin/article?page='+data.pages+'">Last</a></li>';
                }
                $("#pageItem").html(pageRow)
                // // form 
                // for (var loc of data.location){
                //   loca_row += '<option value="'+loc.id_location+'">'+loc.location_name+'</option>'
                // }
                $('#data-article').html(row) 
                // $('#inputtype').html(type_row)
                // $('#inputlocation').html(loca_row)
                if(query!=""){var html = '<div></div>'}else{var html = '<div></div>'}
                $('#page').html(html)
            }
        })
        
    }

    function getDataById(position){
      // hide form msg error
      tempImg = []
      tempDelImg = []
      $('#listinfo').html('')
      $('#previewtxt').text('')
      listFile = []
      listKet = []
      var btnsave=""; 
      // create btn save add / edit
      if(position!=null){
        btnsave = $('<input />', { type:'button',value:'Edit',id:'btnedit',
                  on    : {click:function() { 
                    editdata(dataArticle[position]._id) } } });
        }else{
        btnsave = $('<input />', { type:'button',value :'Add',id:'btnadd',
                  on    : {click:function() { 
                    inputdata() } }  }); 
        $('#msgtxt').html('<div/>')  
      }
      $('#btnaction').html(btnsave);   
        
      //console.log(data);
      if(dataArticle[position]!=null){
        // Image
      if( dataArticle[position].images != null ){
        for( var j in dataArticle[position].images){   
          var imageFiles = dataArticle[position].images[j]
          tempImg.push(imageFiles)
          var fileNameIndex = imageFiles.image.lastIndexOf("/") + 1;
            var filename = imageFiles.image.substr(fileNameIndex);
               var row = '  <tr id="listcount'+j+'">'+
                  '<td><img src="../assets/upload/article/'+filename+'" width="100px" height="100px"></img></td>'+
                  '<td>'+imageFiles.detail+'</td>'+
                  '<td><button id="minusinforow'+j+'" onclick="event.preventDefault(); minusinforow('+j+');" class="btn btn-warning"><i class="fa fa-trash"></i></a></td></tr>'
               $('#listinfo').append(row)     
        }
      }    
        // edit form value
        $("[name=id]").val(dataArticle[position]._id);
        $("[name=title]").val(dataArticle[position].title);
        $("[name=desc]").val(dataArticle[position].desc);
        $("[name=author]").val(dataArticle[position].author);
      }else{
        // input form & img res
        $("[name=id]").val("");
        $("[name=title]").val("");
        $("[name=desc]").val("");
        $("[name=author]").val("");
        // btn upload input
        $('#inputfile').show()
        $('#btnupload').hide();
      } 
    }

    function uploadimg(id,callback){ 
      // when there is a file image, it should be uploaded
      if(listFile.length> 0){
        var formData = new FormData();  
        for( var i =0 ; i< listFile.length;i++){
        formData.append('images[]',listFile[i])
        }
        formData.append('imageCount',listFile.length)   
        formData.append('details',listKet)
        const xhr = new XMLHttpRequest();
        xhr.open('POST', '/admin/article/upload',true);
        xhr.onreadystatechange = function(){
          if(xhr.status == 500){
            console.log(errorMessage);
            return;
          }
          if(xhr.readyState == 4 && xhr.status == 200){
            if(xhr.responseText.includes('Error')){
              console.log(xhr.responseText);
              console.log(errorMessage);
              return;
            }
            var result = JSON.parse(xhr.responseText).result;
            for (var i in result){
              tempImg.push(result[i])
            }
            updateImg(id,tempImg,function(res){
              callback(res)
            })  
          }
        }
        xhr.send(formData)
      }else{
        updateImg(id,tempImg,function(res){
          callback(res)
        })
      }         
    }

    function updateImg(id,img,callback){
      $.ajax({
        type: 'PATCH',
        url: BASE_URL+'/'+id,
        data: {images : img, uploadImg : true },
        crossDomain:true,
        dataType: 'json',
        success: function(data){
          callback(data)
        }
      })
    }

    
    // to clear images table
    $('#btnaddnew').on('click', function(){
      listFile = []
      listKet = []
      tempImg = []
      $('#listinfo').html('')
    })

    // input
    function inputdata(){
      var dataForm = $("#formarticle").serializeArray();
        $.ajax({
          type: 'POST',
          url: BASE_URL,
          data: dataForm,
          crossDomain:true,
          dataType: 'json',
          success: function(data){
            //console.log(data)
              var alt ="";         
              // if form is valid
              if(data.message){
                // Upload Image First
                if(listFile.length > 0){
                  uploadimg(data._id, function(res){
                    msgCall('success',data.message,'#actionmsg','#modal-default')   
                  })
                }else{
                  msgCall('success',data.message,'#actionmsg','#modal-default')   
                }              
              }else{
                alertCall('alert','Something wrong when input data! Please Try again','#msgtxt')
              }  
          },
          error : function(err){
            alertCall('alert',err.responseText,'#msgtxt')
          }
        }) 
          
    }
    
  /* $('#formarticle').on('submit',function(e){
      e.preventDefault();
      var formdata = new FormData($('#formarticle'));
                  formdata.append("action", 'upload');
                  
    }) 
 */
    function editdata(id){
      var dataForm = $("#formarticle").serializeArray(); 
      if(tempDelImg.length > 0){
        deleteSingleFile(tempDelImg,function(res){
          console.log(res.message);
        })
      } 
      $.ajax({
            type: 'PATCH',
            url: BASE_URL+'/'+id,
            data: dataForm,
            crossDomain:true,
            dataType: 'json',
            success: function(data){
                //console.log(data)
                var alt ="";         
                // if form is valid
                if(data.message){
                    uploadimg(id, function(res){
                      msgCall('success',res,'#actionmsg','#modal-default')   
                    })                              
                }else{
                  alertCall('alert','Something wrong when input data! Please Try again','#msgtxt')
                }  
            },
            error : function(err){
              alertCall('alert',err.responseText,'#msgtxt')
            }
          })   
    }
  
    function deletedata(id){
      //console.log(dataArticle[id].images);
        if(dataArticle[id].images.length > 0){
          deleteFiles(id,function(res){
            console.log(res.message);
          })
        }
        $.ajax({
          type: 'DELETE',
          url: BASE_URL+'/'+dataArticle[id]._id,
          crossDomain:true,
          dataType : 'json',
          success: function(data){
            if(data.message){
              msgCall('success',data.message,'#actionmsg','#modal-danger-delete')
            }else{
              msgCall('alert','Unsuccessfully Delete Data','#actionmsg','#modal-danger-delete')
            }
          }
        })
    }

    function deleteSingleFile(data,callback){
        var formData = new FormData();
        formData.append('images', JSON.stringify(data) )   
        $.ajax({
          type: 'POST',
          url: '/admin/article/delete',
          data : formData,
          processData: false,
          contentType: false,
          crossDomain:true,
          dataType : 'json',
          success: function(data){
            callback(data)
          }
        })
    }

    function deleteFiles(id,callback){
        var formData = new FormData();
        formData.append('images', JSON.stringify(dataArticle[id].images) )   
        $.ajax({
          type: 'POST',
          url: '/admin/article/delete',
          data : formData,
          processData: false,
          contentType: false,
          crossDomain:true,
          dataType : 'json',
          success: function(data){
            callback(data)
          }
        })
    }
    // modal delete action
  $('#modal-danger-delete').on('show.bs.modal', function(e) {
    var id = $(e.relatedTarget).data('id');
    $('#yesbtn').on('click',function(){
        deletedata(id)
    })
    });
    // function helper
    $('#search-text').keyup(function(){
        var search = $(this).val();
    if(search != "")
    {
     getData(search)
    }
    else
    {
      getData();
    } 
    });  

    function alertCall($type,$msg,$target){
      if($type == 'alert'){
        alt= '<div class="alert alert-danger alert-dismissible">'+
                      '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
                      '<h5><i class="icon fas fa-ban"></i> Alert!</h5>'+$msg+
                    '</div>'
      }else if($type == 'success'){
        alt= '<div class="alert alert-success alert-dismissible">'+
                      '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
                      '<h5><i class="icon fas fa-check"></i> Success!</h5>'+$msg+
                    '</div>'
      }
      $($target).html(alt) 
    }

    function msgCall(type,msg,target,modaltarget){
      if(type == 'alert'){
        ret= ' <div class="card card-danger"><div class="card-header"><h4 class="card-title">'+msg+'</h4></div></div>'
      }else if(type == 'success'){
        ret= ' <div class="card card-success"><div class="card-header"><h4 class="card-title">'+msg+'</h4></div></div>'
      }
      $(target).html(ret)
      getData()
      $(modaltarget).modal('hide');
    }