var BASE_URL = 'https://sweetlanka-server.herokuapp.com/api/destination'
const params = new URLSearchParams(window.location.search);
var id = params.get('id')
var filename="1609808198343_Sirgiya_Negombo.jpeg"
var imgArray = []
var imgRow = ""

$(function() {

    getDataById(id,function (data) {
        console.log(data); 
        $('#txtDest').text(data.name)
        $('#txtDesc').text(data.desc)
        var imgs = data.images 
        for( var i in imgs){
            var img = imgs[i].image.lastIndexOf("/") + 1;
            imgArray.push(imgs[i].image.substr(img))
            imgRow +=  '<div class="column" >'+
            '<img src="../assets/upload/destination/'+imgArray[i]+'" alt="Nature" onclick="myFunction(this);">'+
            '</div>'           
        }
        $('#img').html(imgRow)
        console.log(imgRow);
    });
});

// imgRow += '<div class="slideshow__slide js-slider-home-slide is-current" data-slide="'+i+'">'+
//         '<div class="slideshow__slide-background-parallax background-absolute js-parallax" data-speed="-1" data-position="top" data-target="#js-header">'+
//             '<div class="slideshow__slide-background-load-wrap background-absolute">'+
//                 '<div class="slideshow__slide-background-load background-absolute">'+
//                     '<div class="slideshow__slide-background-wrap background-absolute">'+
//                         '<div class="slideshow__slide-background background-absolute">'+
//                             '<div class="slideshow__slide-image-wrap background-absolute">'+
//                                 '<div class="slideshow__slide-image background-absolute" style="background-image: url(../assets/upload/destination/'+imgArray[i]+');"></div>'+
//                            '</div></div></div></div></div></div>'+
//         '<div class="slideshow__slide-caption">'+
//            ' <div class="slideshow__slide-caption-text">'+
//                 '<div class="container js-parallax" data-speed="2" data-position="top" data-target="#js-header">'+
//                     '<h1 class="slideshow__slide-caption-title">Everything broken can be repaired</h1>'+
//                     '<a class="slideshow__slide-caption-subtitle -load o-hsub -link" href="#">'+
//                         '<span class="slideshow__slide-caption-subtitle-label">See how</span>'+
//                    '</a></div></div></div></div> '
    
    

function getDataById(id,callback){
    // hide form msg error
    $.ajax({
        type:'GET',
        // https://sweetlanka-server.herokuapp.com/api/destination
        // http://localhost:3000/api/destination
        url: BASE_URL+'/'+id,
        dataType: 'json',
        success: function(data){
            callback(data)
            
        }
    })
  }