var pathname = window.location.pathname.split('/');
    var BASE_URL = 'https://sweetlanka-server.herokuapp.com/api/intent'
    // https://sweetlanka-server.herokuapp.com/api/intent
     // http://localhost:3000/api/intent

    var dataIntent = [];
    var dataTrainingData = []


    getData();    


    function getData(query=""){ 
      const params = new URLSearchParams(window.location.search);
      var keyword= params.get("search");
      if(query!= ""){
        keyword = query
      }
      var page = params.get("page");
      var limit = params.get("limit");
        $.ajax({
            type:'GET',
            // https://sweetlanka-server.herokuapp.com/api/intent
            // http://localhost:3000/api/intent
            url: BASE_URL+'?search='+keyword+'&page='+page+'&limit='+limit,
            dataType: 'json',
            success: function(data){
                console.log(data);
                var row = '';var loca_row='';
                // table
                for(var i=0;i<data.length;i++){
                    dataIntent[i] = {
                      _id : data[i]._id,
                      response : data[i].response,
                      name : data[i].name,
                      training_data : data[i].training_data,
                      createdAt : data[i].createdAt,
                      updatedAt : data[i].updatedAt
                    }
                    row += '<tr>'+
                      '<td>'+data[i]._id+'</td>'+
                      '<td>'+data[i].name+'</td>'+
                      '<td>'+data[i].response.slice(0, 20) + (data[i].response.length > 20 ? "..." : "")+'</td>'+
                      '<td>'+'<button type="button" data-toggle="modal" onclick="getDataById('+i+')"  data-target="#modal-default" style="margin-right:5px" class="btn btn-outline-primary"><i class="fas fa-edit"></i></button>'+
                      '<button type="button" data-toggle="modal" data-target="#modal-danger-delete" data-id="'+i+'" id="btndel" class="btn btn-outline-danger"><i class="fas fa-trash"></i></button>'+
                      '</td>'+
                    '</tr>'
                }
                
                // Pagination
                // var pageNumber = (Number(data.current) > 5 ? Number(data.current) - 4 : 1)
                // var pageRow = '';
                // if(data.current == 1){
                //   pageRow += '<li class="page-item disabled" id="pageFirst"><a class="page-link">First</a></li>';
                // }else{
                //   pageRow += '<li class="page-item" id="pageFirst"><a class="page-link" href="'+window.location.origin+'/admin/intent?page=1">First</a></li>';
                // }
                // if (pageNumber !== 1) {
                //   pageRow += '<li class="page-item disabled" id="pageDotf"><a class="page-link">...</a></li>';
                // }
                // for (; pageNumber <= (Number(data.current) + 4) && pageNumber <= data.pages; pageNumber++) {
                //   if (pageNumber == data.current) {
                //     pageRow += '<li class="page-item active" ><a class="page-link" href="'+window.location.origin+'/admin/intent?page='+pageNumber+'">'+pageNumber+'</a></li>';
                //   }else{
                //     pageRow += '<li class="page-item" ><a class="page-link" href="'+window.location.origin+'/admin/intent?page='+pageNumber+'">'+pageNumber+'</a></li>';
                //   }
                //   if (pageNumber == Number(data.current) + 4 && pageNumber < data.pages) {
                //    pageRow += '<li class="page-item disabled" id="pageDotl"><a class="page-link">...</a></li>';
                //   }
                // }
                
                // if (data.current == data.pages) {
                //   pageRow += '<li class="page-item disabled" id="pageLast"><a class="page-link">Last</a></li>';
                // }else{
                //   pageRow += '<li class="page-item" id="pageLast"><a class="page-link" href="'+window.location.origin+'/admin/intent?page='+data.pages+'">Last</a></li>';
                // }
                // $("#pageItem").html(pageRow)
                // // form 
                // for (var loc of data.location){
                //   loca_row += '<option value="'+loc.id_location+'">'+loc.location_name+'</option>'
                // }
                $('#data-intent').html(row) 
                // $('#inputtype').html(type_row)
                // $('#inputlocation').html(loca_row)
                if(query!=""){var html = '<div></div>'}else{var html = '<div></div>'}
                $('#page').html(html)
            }
        })
        
    }

    function getDataById(position){
      // hide form msg error
      $('#listinfo').html('')
      $('#previewtxt').text('')
      dataTrainingData = []
      var btnsave=""; 
      // create btn save add / edit
      if(position!=null){
        btnsave = $('<input />', { type:'button',value:'Edit',id:'btnedit',
                  on    : {click:function() { 
                    editdata(dataIntent[position]._id) } } });
        }else{
        btnsave = $('<input />', { type:'button',value :'Add',id:'btnadd',
                  on    : {click:function() { 
                    inputdata() } }  }); 
        $('#msgtxt').html('<div/>')  
      }
      $('#btnaction').html(btnsave);   
        
      //console.log(data);
      if(dataIntent[position]!=null){ 
        // edit form value
        dataTrainingData = dataIntent[position].training_data
        $("[name=name]").val(dataIntent[position].name);
        $("#inputResponse").val(dataIntent[position].response);
        $("[name=training_data]").val(dataIntent[position].training_data);
        if(dataTrainingData.length > 0){
          for( var i in dataTrainingData){
            var row = '  <tr id="listcount'+i+'">'+
                  '<td>'+dataTrainingData[i].sentence+'</td>'+
                  '<td><button id="minusinforow'+i+'" onclick="event.preventDefault(); minusinforow('+i+');" class="btn btn-warning"><i class="fa fa-trash"></i></a></td></tr>'
                  $('#listinfo').append(row)
          }
        }
        
      }else{
        // input form & img res
        $("[name=name]").val("");
        $("[name=response]").val("");
      } 
    }

    $('#addinforow').on('click',function(event){
      event.preventDefault();
      var $div = $('tr[id^="listcount"]:last');
      if($div.length > 0 ){
          var num = parseInt( $div.prop("id").match(/\d+/g), 10 ) +1;
      }else{
          var num = 0;
      }
      var sentence = $('#inputSentence').val()
      if( sentence != ""){
        dataTrainingData.push({sentence : sentence})
        var row = '  <tr id="listcount'+num+'">'+
                  '<td>'+sentence+'</td>'+
                  '<td><button id="minusinforow'+num+'" onclick="event.preventDefault(); minusinforow('+num+');" class="btn btn-warning"><i class="fa fa-trash"></i></a></td></tr>'
        $('#listinfo').append(row)
        $('#inputSentence').val('')
      }else{
        alert('Please insert the sentence!')
      }
    })
    
    // to clear images table
    $('#btnaddnew').on('click', function(){
      dataTrainingData = []
      $('#listinfo').html('')
    })

    function minusinforow(id){
      $('#listcount'+id).remove()
      if(dataTrainingData.length > 0 ){
        dataTrainingData.splice(id,1)   
      }
      console.log(dataTrainingData);
    }

    // input
    function inputdata(){
      var dataForm = $("#formintent").serializeArray(); 
      var resp = $('#inputResponse').val()
      dataForm.push({ name : 'response', value : resp})
      dataForm.push({ name : 'training_data', value : JSON.stringify(dataTrainingData)})
      //console.log(dataForm);
      $.ajax({
        type: 'POST',
        url: BASE_URL,
        data: dataForm,
        crossDomain:true,
        dataType: 'json',
        success: function(data){
            //console.log(data)
            if(data.message){               
                  msgCall('success',data.message,'#actionmsg','#modal-default')                            
            }else{
              alertCall('alert','Something wrong when input data! Please Try again','#msgtxt')
            }  
        },
        error : function(err){
          alertCall('alert',err.responseText,'#msgtxt')
        }
      })
          
    }
    
  /* $('#formintent').on('submit',function(e){
      e.preventDefault();
      var formdata = new FormData($('#formintent'));
                  formdata.append("action", 'upload');
                  
    }) 
 */
    function editdata(id){
      var dataForm = $("#formintent").serializeArray(); 
      var resp = $('#inputResponse').val()
      dataForm.push({ name : 'response', value : resp})
      dataForm.push({ name : 'training_data', value : JSON.stringify(dataTrainingData)})
      //console.log(dataForm);
      $.ajax({
            type: 'PATCH',
            url: BASE_URL+'/'+id,
            data: dataForm,
            crossDomain:true,
            dataType: 'json',
            success: function(data){
                //console.log(data)
                if(data.message){               
                      msgCall('success',data.message,'#actionmsg','#modal-default')                            
                }else{
                  alertCall('alert','Something wrong when input data! Please Try again','#msgtxt')
                }  
            },
            error : function(err){
              alertCall('alert',err.responseText,'#msgtxt')
            }
          })   
    }
  
    function deletedata(id){
      //console.log(dataIntent[id].images);
        $.ajax({
          type: 'DELETE',
          url: BASE_URL+'/'+dataIntent[id]._id,
          crossDomain:true,
          dataType : 'json',
          success: function(data){
            if(data.message){
              msgCall('success',data.message,'#actionmsg','#modal-danger-delete')
            }else{
              msgCall('alert','Unsuccessfully Delete Data','#actionmsg','#modal-danger-delete')
            }
          }
        })
    }


    // modal delete action
  $('#modal-danger-delete').on('show.bs.modal', function(e) {
    var id = $(e.relatedTarget).data('id');
    $('#yesbtn').on('click',function(){
        deletedata(id)
    })
    });
    // function helper
    $('#search-text').keyup(function(){
        var search = $(this).val();
    if(search != "")
    {
     getData(search)
    }
    else
    {
      getData();
    } 
    });  

    function alertCall($type,$msg,$target){
      if($type == 'alert'){
        alt= '<div class="alert alert-danger alert-dismissible">'+
                      '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
                      '<h5><i class="icon fas fa-ban"></i> Alert!</h5>'+$msg+
                    '</div>'
      }else if($type == 'success'){
        alt= '<div class="alert alert-success alert-dismissible">'+
                      '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
                      '<h5><i class="icon fas fa-check"></i> Success!</h5>'+$msg+
                    '</div>'
      }
      $($target).html(alt) 
    }

    function msgCall(type,msg,target,modaltarget){
      if(type == 'alert'){
        ret= ' <div class="card card-danger"><div class="card-header"><h4 class="card-title">'+msg+'</h4></div></div>'
      }else if(type == 'success'){
        ret= ' <div class="card card-success"><div class="card-header"><h4 class="card-title">'+msg+'</h4></div></div>'
      }
      $(target).html(ret)
      getData()
      $(modaltarget).modal('hide');
    }


    
    