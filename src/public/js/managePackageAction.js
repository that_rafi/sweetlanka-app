var BASE_URL = 'https://sweetlanka-server.herokuapp.com/api/packagetour'
const params = new URLSearchParams(window.location.search);
    // https://sweetlanka-server.herokuapp.com/api/packagetour
     // http://localhost:3000/api/packagetour
var pkgid = params.get("id");
var tempItin = []
var tempDest = []
$('#bookid').hide()
$('#content').hide()

$(function() {
  // when form date is changed
  $('#inputdate').on('change',function(event){
    $('#itinerary').html('')
    additinrow()
  })
  // if there is a book id
  var bookid = params.get("bookid");
            if(bookid != null){
                $('#bookid').show()
  }
  // get data for edit and add
  getPackageDataById(pkgid)
})


// get data
function getPackageDataById(id){
    $.ajax({
        type:'GET',
        url: BASE_URL+'/'+id,
        dataType: 'json',
        success: function(data){
          console.log(data)
          $('#content').show()
          $('#loading').hide() 
          tempDest = data.destination
          var btnsave="";
          if(pkgid != null){    
                // generate Itinerary
                tempItin = data.package.itinerary
                generateFormItin(tempItin,data.package.start_date)
                // edit form value
                $("[name=name]").val(data.package.name);             
                $("[name=desc]").val(data.package.desc);
                $("[name=pickup_location]").val(data.package.pickup_location);
                $("[name=base_price]").val(data.package.base_price)
                $("[name=categoryId]").val(data.package.categoryId)
                $("[name=hotel_star]").val(data.package.hotel_star)
                if(data.package.status) {
                  $("[name=status]").val( '1')
                }else{
                  $("[name=status]").val( '0')
                }             
                $("[name=start_date]").val(new Date(data.package.start_date).toISOString().split('T')[0])
                var stringTags = data.package.tags.toString();
                $("#inputtags").tagsinput('add', stringTags);
                // facility include exclude  
                // // facility
                // var cbleft=""; var cbright="";
                // if(data.package.bookid!="" ){ // if there's book id then generate itin depends on how many duration user input
                //   // set tags and create new tag if admin want to
                            
                // }else{     // if there is pkg id instead
                //   if(data.package.pkgid!=""){    

                //   }else{
                    
                //   }
                // }               
                // create btn save add / edit
                btnsave = $('<input />', { type:'button',class:'btn btn-primary',value:'Edit Package',id:'btnedit',on    : {click:function() { 
                editdata(pkgid) } } });
            }else{
                $('#content').show()
                $('#loading').hide() 
                $("[name=id]").val("");
                $("[name=name]").val("");             
                $("[name=desc]").val("");
                $("[name=tags]").val("")
                $("[name=transportation]").val("");
                // create btn save add / edit
                btnsave = $('<input />', { type:'button',class:'btn btn-primary',value :'Add Package',id:'btnadd', on    : {click:function() { 
                  inputdata() } }  }); 
            }
            $('#btnaction').html(btnsave);
             // set select
             generateSelect(data) 
        }
      })
  }

  function generateSelect(data){
     // category
     var category_row = []
     for ( var i in data.category){
       category_row += '<option value="'+data.category[i]._id+'">'+data.category[i].name+'</option>'
     }
     $('#inputcategory').html(category_row)
     if(data.package != null){
          // destination
          var dest_row = []
          for ( var i in data.destination){
            dest_row += '<option value="'+data.destination[i]._id+'">'+data.destination[i].name+'</option>'
          }    
          if(data.package.itinerary.length>0){
            for ( var i in data.package.itinerary){
              $('#inputdestination'+i).html(dest_row)
            }
            for( var i in data.package.itinerary){
            $('#inputdestination'+i).val(data.package.itinerary[i].destinationId)
            }
            }  
     }
     
  }

function generateFormItin(itinerary,startdate){
  var itin_row = []
  var separator=""
  var days = []
  var thedate = new Date(startdate).toISOString().split('T')[0]
  if(thedate!=""){ // if there is a startdate ( bookid )
  var date = new Date(thedate);
  days = incrementDate(date,itinerary.length)
  }
  if(itinerary.length == 0){
      itin_row +=  rowItin(0,days[0],separator,"","","")     
  }else{
    for( var i =0 ; i<itinerary.length; i++){
      if(days[i]==undefined){
        days[i]=""
      }else{
        separator=" - "
      }
      itin_row +=  rowItin(i,days[i],separator,itinerary[i].date,itinerary[i].title,itinerary[i].program)       
    }   
  }
  $('#itinerary').append(itin_row) 
}

function rowItin(day,stringday,separator,date,title,program){
  return '<div id="formitineraryday'+(day)+'">'+
  '<label for="dayanddate">Day '+(day+1)+separator+stringday+' </label>'+
  '<input type="hidden" name="itinerary['+day+'][date]" id="itinerarydate'+day+'" value="'+date+'"/>'+
  '<div class="form-group">'+
  '<label for="exampleInputtitle">Title</label>'+
  '<input type="text" name="itinerary['+day+'][title]" id="title'+day+'" value="'+title+'" class="form-control" placeholder="Enter Title...." ></div>'+
  '<div class="form-group">'+
  '<label>Destination</label>'+
  '<select name="itinerary['+day+'][destinationId]" id="inputdestination'+day+'" class="custom-select"></select></div>'+
  '<div class="form-group">'+
  '<label for="exampleInputProgram">Program</label>'+
  '<textarea name="itinerary['+day+'][program]" id="program'+day+'" class="form-control" rows="3" placeholder="Enter Program detail...">'+program+'</textarea></div>'+
  '<div class="form-group"><button type="button" data-id="'+day+'" id="minusinforow'+day+'" onclick="event.preventDefault(); minusinforow('+day+');" class="btn btn-outline-danger"><i class="fas fa-trash"></i></button>'+
  '</div></div>'
}

function minusinforow(id){
  $('#formitineraryday'+id).remove()
  tempItin.splice(id,1)
}

function additinrow(){
  var $div = $('div[id^="formitineraryday"]:last');
  var nextid = 0;
  var lastid = 0;
  var day = [];
    if($div.length > 0 ){
        var lastid = parseInt( $div.prop("id").match(/\d+/g), 10 );
        var nextid = lastid +1;
        var lastdate = $('#itinerarydate'+lastid).val();
        var thedate = new Date(lastdate).toISOString().split('T')[0]
        var date = new Date(thedate);
        day = incrementDate(date,2)
    }else{
        var lastdate = $('#inputdate').val()
        if(lastdate!= ''){
          var thedate = new Date(lastdate).toISOString().split('T')[0]
          var date = new Date(thedate);
          day = incrementDate(date,1)
        }else{
          alert('Please input start date form!');
        }
        
    }
    if(lastdate!= ''){
      var nextdate = new Date(date).toISOString().split('T')[0];
      var stringnxtdate =  day[day.length-1] ;
      $('#itinerary').append(rowItin(nextid,stringnxtdate," - ",nextdate,"","")) 
      var dest_row = []
      for ( var i in tempDest){
       dest_row += '<option value="'+tempDest[i]._id+'">'+tempDest[i].name+'</option>'
      }
      $('#inputdestination'+nextid).html(dest_row)
    }
      
   
}

function formatDate(initdate){
  return initdate.toLocaleDateString('en-GB', {day : 'numeric',month : 'long',year : 'numeric'}).split(' ').toString().replace(','," "); 
}
function incrementDate(next_day,duration){
  var dates = [];
  dates[0]= formatDate(next_day); 
  for(var i=1;i<duration;i++){
  next_day.setDate(next_day.getDate() + 1);
  dates[i]=formatDate(next_day); 
  }
  return dates;
}

// input
function inputdata(){
  var dataForm = $("#formpackage").serializeArray();
  var dataFormItin = $("#formitinerary").serializeArray(); 
  dataForm.push({ name : 'facility' , value : "facility,adak"})
  dataForm.push({ name : 'status' , value : true})
  dataForm.push({ name : 'tags' , value : $('#inputtags').val()})
  dataFormItin.push({name : 'updateitinerary', value : true}) 

  $.ajax({
        type: 'POST',
        url: BASE_URL,
        data: dataForm,
        crossDomain:true,
        dataType: 'json',
        success: function(data){
            //console.log(data);     
            // if form is valid
            if(data.message){
              if(dataFormItin.length > 0){
                updateItinerary(data._id,dataFormItin,function(res){
                  alert(data.message)
                  window.location = '/admin/package'
                })   
              }                       
            }else{
              alertCall('alert','Something wrong when input data! Please Try again','#msgtxtpackage')
            }  
         },
        error : function(err){
          alertCall('alert',err.responseText,'#msgtxtpackage')
          $("html, body").scrollTop(0);
        }
      })   
        
  }


  function editdata(id){
    var dataForm = $("#formpackage").serializeArray();
    var dataFormItin = $("#formitinerary").serializeArray(); 
    dataForm.push({ name : 'facility' , value : "facility,adak"}) 
    dataForm.push({ name : 'tags' , value : $('#inputtags').val()})
    dataFormItin.push({name : 'updateitinerary', value : true})

    $.ajax({
          type: 'PATCH',
          url: BASE_URL+'/'+id,
          data: dataForm,
          crossDomain:true,
          dataType: 'json',
          success: function(data){
              //console.log(data);     
              // if form is valid
              if(data.message){
                updateItinerary(id,dataFormItin,function(res){
                  alertCall('success',res.message,'#msgtxtpackage')
                  $("html, body").scrollTop(0);
                })                           
              }else{
                alertCall('alert','Something wrong when input data! Please Try again','#msgtxtpackage')
              }  
           },
          error : function(err){
            alertCall('alert',err.responseText,'#msgtxtpackage')
            $("html, body").scrollTop(0);
          }
        })   
  }

function updateItinerary(id,itinerary,callback){
  $.ajax({
    type: 'PATCH',
    url: BASE_URL+'/'+id,
    data: itinerary,
    crossDomain:true,
    dataType: 'json',
    success: function(data){
        callback(data)
     },
    error : function(err){
      alertCall('alert',err.responseText,'#msgtxtpackage')
    }
  }) 
} 

  

function generateDays(startdate,duration){ 
    if(duration!=""){
    var itin_row ="";var days=[];var separator=""
    if(startdate!=""){ // if there is a startdate ( bookid )
      var date = new Date(startdate);
      days = incrementDate(date,duration)
    }
    for(var i=0;i<duration;i++){
      if(days[i]==undefined){
        days[i]=""
      }else{
        separator=" - "
      }
      itin_row += '<div id="formitineraryday'+(i+1)+'">'+
      '<label for="dayanddate">Day '+(i+1)+separator+days[i]+'</label>'+
      '<input type="hidden" name="itinerary['+i+'][day]" id="day'+i+'" value="'+(i+1)+'"/>'+
      '<input type="hidden" name="itinerary['+i+'][id_package]" id="packageid'+i+'" />'+
      '<input type="hidden" name="itinerary['+i+'][id_itinerary]" id="itineraryid'+i+'" />'+
      '<div class="form-group">'+
      '<label for="exampleInputtitle">Title</label>'+
      '<input type="text" name="itinerary['+i+'][title]" id="title'+i+'" class="form-control" placeholder="Enter Title...." ></div>'+
      '<div class="form-group">'+
      '<label for="exampleInputProgram">Program</label>'+
      '<textarea name="itinerary['+i+'][program]" id="program'+i+'" class="form-control" rows="3" placeholder="Enter Program detail..."></textarea></div>'+
      '<div class="form-group">'+
      '<label>Destination</label>'+
      '<select name="itinerary['+i+'][destinationId]" id="inputdestination'+i+'" class="custom-select"></select></div>'+
      '<div class="form-group">'+
        '<label>Room</label>'+
        '<select name="itinerary['+i+'][id_room]" id="inputroom'+i+'" class="custom-select"></select></div>'+
    ' <div class="form-group">'+
        '<label>Estimated Price</label>'+
        '<div class="input-group">'+
            '<input name="estptice" type="text" class="form-control">'+
            '<div class="input-group-append">'+
                '<span class="input-group-text">0</span>'+
        '</div></div></div></div>'
    }
    $('#itinerary').html(itin_row)
    }
    
  }

  function formatDate(initdate){
    return initdate.toLocaleDateString('en-GB', {day : 'numeric',month : 'long',year : 'numeric'}).split(' ').toString().replace(','," "); 
  }
  function incrementDate(next_day,duration){
    var dates = [];
    dates[0]= formatDate(next_day); 
    for(var i=1;i<duration;i++){
    next_day.setDate(next_day.getDate() + 1);
    dates[i]=formatDate(next_day); 
    }
    return dates;
  }

  function alertCall($type,$msg,$target){
    if($type == 'alert'){
      alt= '<div class="alert alert-danger alert-dismissible">'+
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
                    '<h5><i class="icon fas fa-ban"></i> Alert!</h5>'+$msg+
                  '</div>'
    }else if($type == 'success'){
      alt= '<div class="alert alert-success alert-dismissible">'+
                    '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
                    '<h5><i class="icon fas fa-check"></i> Success!</h5>'+$msg+
                  '</div>'
    }
    $($target).html(alt) 
  }

  function msgCall(type,msg,target,modaltarget){
    if(type == 'alert'){
      ret= ' <div class="card card-danger"><div class="card-header"><h4 class="card-title">'+msg+'</h4></div></div>'
    }else if(type == 'success'){
      ret= ' <div class="card card-success"><div class="card-header"><h4 class="card-title">'+msg+'</h4></div></div>'
    }
    $(target).html(ret)
    getData()
    $(modaltarget).modal('hide');
  }