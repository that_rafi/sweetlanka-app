var pathname = window.location.pathname.split('/');
var BASE_URL = 'https://sweetlanka-server.herokuapp.com/api/reservation'
var pageno = pathname[4];
var dataReservation = [];
var dataPackages = []
    
    $( window ).on( "load", function() {
        getData();
    });

    $('#unselectpkg').on('click',function(){
      setOffSelect("#inputduration",false)
      $('#pkginfo').attr("hidden",true);
      $('#unselectpkg').attr("hidden",true)
      $('#inputpackage').val(0)
      $('#inputduration').val($('#durationHidden').val())
    })

    $('#btnedit').on('click',function(){
      editdata(1)
    })


    $('#inputpackage').on('change',function(){
          var res = $(this).val()
          var position = $(this).find(':selected').data('id');
          var newval = $('#durationHidden').val()
          if(res != 0){
            setOffSelect("#inputduration",true)
            $('#unselectpkg').attr("hidden",false)
            $('#pkginfo').attr("hidden",false); 
            $('#infopkgid').text(dataPackages[position]._id) 
            $('#infopkgdesc').text(dataPackages[position].desc)  
            $('#infopkgtrans').text(dataPackages[position].categoryId.transportation)
            $('#infopkgtags').text(dataPackages[position].tags)  
            newval = dataPackages[position].itinerary.length      
          }else{
            setOffSelect("#inputduration",false)
            $('#pkginfo').attr("hidden",true);
            $('#unselectpkg').attr("hidden",true)
          } 
          $("#inputduration").val(newval)

        })

    // function editpackage(id){
    //   $.redirect(base+'admin/package/action', {'idpkg': id});
    // }

    $('#main').attr('hidden',false);
          $('#loading').attr('hidden',true);
          
    function getData(query=""){ 
            const params = new URLSearchParams(window.location.search);
          $('#main').attr('hidden',false);
          $('#loading').attr('hidden',true);
            var keyword= params.get("search");
            if(query!= ""){
                keyword = query
            }
            var page = params.get("page");
            var limit = params.get("limit");
        $.ajax({
            type:'GET',
            url: BASE_URL+'?search='+keyword+'&page='+page+'&limit='+limit,
            dataType: 'json',
            success: function(data){
                console.log(data);
                dataPackages = data.packages
                 var row = '';
                // table
                for(var i in data.reservations){
                    dataReservation[i] = {
                        _id : data.reservations[i]._id,
                        packageId : data.reservations[i].packageId,
                        customer : data.reservations[i].customer,
                        duration : data.reservations[i].duration,
                        no_adult : data.reservations[i].no_adult,
                        no_child : data.reservations[i].no_child,
                        notes : data.reservations[i].notes,
                        preferences : data.reservations[i].preferences,
                        start_date : data.reservations[i].start_date,
                        status : data.reservations[i].status,
                        createdAt : data.reservations[i].createdAt,
                        updatedAt : data.reservations[i].updatedAt
                      }
                    var pkg = ""; var btnact=""
                    if(data.reservations[i].packageId){
                        pkg = '<a onclick="editpackage('+data.reservations[i].packageId+')"><font color="blue"><u>'+data.reservations[i].packageId+'</u></font></a>';
                        btnact = '<button type="button" onclick="printpdf('+data.reservations[i].packageId+')" id="btnprint" class="btn btn-outline-info" style="margin-right:5px"><i class="fas fa-print"></i></button>'+
                      '<button type="button" id="btnemail"  class="btn btn-outline-primary"><i class="fas fa-envelope"></i></button>'
                    }else{
                        pkg = 'No Package!'
                    }   
                    row += '<tr>'+
                      '<td>'+data.reservations[i]._id+'</td>'+
                      '<td>'+data.reservations[i].customer.fullname+'</td>'+
                      '<td>'+pkg+'</td>'+ 
                      '<td>'+data.reservations[i].updatedAt+'</td>'+
                      '<td>'+data.reservations[i].status+'</td>'+               
                      '<td>'+'<button type="button" data-toggle="modal" onclick="getDataById('+i+')" data-target="#modal-default" style="margin-right:5px" class="btn btn-outline-primary"><i class="fas fa-edit"></i></button>'+
                      '<button type="button" data-toggle="modal" data-target="#modal-danger-delete" data-id="'+data.reservations[i]._id+'" id="btndel" style="margin-right:5px" class="btn btn-outline-danger"><i class="fas fa-trash"></i></button>'+
                      btnact
                      '</td>'+
                    '</tr>'
                }
                // Pagination
                var pageNumber = (Number(data.current) > 5 ? Number(data.current) - 4 : 1)
                var pageRow = '';
                if(data.current == 1){
                  pageRow += '<li class="page-item disabled" id="pageFirst"><a class="page-link">First</a></li>';
                }else{
                  pageRow += '<li class="page-item" id="pageFirst"><a class="page-link" href="'+window.location.origin+'/admin/destination?page=1">First</a></li>';
                }
                if (pageNumber !== 1) {
                  pageRow += '<li class="page-item disabled" id="pageDotf"><a class="page-link">...</a></li>';
                }
                for (; pageNumber <= (Number(data.current) + 4) && pageNumber <= data.pages; pageNumber++) {
                  if (pageNumber == data.current) {
                    pageRow += '<li class="page-item active" ><a class="page-link" href="'+window.location.origin+'/admin/destination?page='+pageNumber+'">'+pageNumber+'</a></li>';
                  }else{
                    pageRow += '<li class="page-item" ><a class="page-link" href="'+window.location.origin+'/admin/destination?page='+pageNumber+'">'+pageNumber+'</a></li>';
                  }
                  if (pageNumber == Number(data.current) + 4 && pageNumber < data.pages) {
                   pageRow += '<li class="page-item disabled" id="pageDotl"><a class="page-link">...</a></li>';
                  }
                }
                
                if (data.current == data.pages) {
                  pageRow += '<li class="page-item disabled" id="pageLast"><a class="page-link">Last</a></li>';
                }else{
                  pageRow += '<li class="page-item" id="pageLast"><a class="page-link" href="'+window.location.origin+'/admin/destination?page='+data.pages+'">Last</a></li>';
                }
                $("#pageItem").html(pageRow)
                $('#data-booking').html(row) 
                //
                if(query!=""){var html = '<div></div>'}else{var html = '<div></div>'}
                $('#page').html(html)
            } 
        })
        
    }

    function getDataById(position){
      // hide form msg error
      var pkg_row='<option value="0" id="itin0" >-- Select Package --</option>';
      var stat_row='';var dur_row = "";var pack_row= '<option value="0" selected>Select Package</option>'
      $('#previewtxt').text('')
      // prepare form duration
      for (var i=1;i<15;i++){
        dur_row += '<option value="'+i+'">'+i+'</option>'
      }
      $('#inputduration').html(dur_row)
      // prepare form packages
      //console.log(dataPackages);
      for(var i in dataPackages){
        pack_row += '<option value="'+dataPackages[i]._id+'"  data-id="'+i+'"  >'+dataPackages[i].name+'</option>'
      }
      $('#inputpackage').html(pack_row)
      if(dataReservation[position]!=null){
        if(dataReservation[position].packageId != null){
          $("[name=packageId]").val(dataReservation[position].packageId);
          setOffSelect("#inputduration",true)
        }else{
          setOffSelect("#inputduration",false)
        }        
        $("#newpkg").html('<a onclick="addnewpkg('+dataReservation[position]._id+')" ><font color="blue"><u>Create New Package</u></font></a>')
        $('#_id').val(dataReservation[position]._id)
        $('#position').val(position)
        // customer
        $("[name=customerId]").val(dataReservation[position].customer._id);
        $("[name=fullname]").val(dataReservation[position].customer.fullname);
        $("[name=email]").val(dataReservation[position].customer.email);
        $("[name=location]").val(dataReservation[position].customer.location);
        $("[name=phone_number]").val(dataReservation[position].customer.phone_number);
        // end customer
        $("[name=duration]").val(dataReservation[position].duration);
        $("#durationHidden").val(dataReservation[position].duration);
        $("[name=no_child]").val(dataReservation[position].no_child);
        $("[name=no_adult]").val(dataReservation[position].no_adult);
        $("[name=start_date]").val(new Date(dataReservation[position].start_date).toISOString().split('T')[0]);
        $("[name=status]").val(dataReservation[position].status);
        $("[name=notes]").val(dataReservation[position].notes);
        $("#inputtags").tagsinput('removeAll');
        if(dataReservation[position].preferences!= null){
          var preferences = dataReservation[position].preferences.split(',');
          for(var i in preferences){
            $("#inputtags").tagsinput('add', preferences[i],preferences[i]);
          }
        }
        
        

      }else{
        $('#_id').val("")
        $('#position').val("")
        $("[name=packageId]").val("");
        $("[name=customerId]").val("");
        $("[name=duration]").val("");
        $("[name=no_child]").val("");
        $("[name=no_adult]").val("");
        $("[name=start_date]").val("");
        $("[name=status]").val("");
        $("[name=notes]").val("");
      }
    }
    // edit
    function editdata(){     
      var dataForm = $("#formbooking").serializeArray();
      dataForm.push({name : 'preferences' , value : $('#inputtags').val()})
      var position = $('#position').val();
      var id = $('#_id').val();

      console.log(dataForm);
      $.ajax({
        type: 'PATCH',
        url: BASE_URL+'/'+id,
        data: dataForm,
        crossDomain:true,
        dataType: 'json',
        success: function(data){
            //console.log(data)
            var alt ="";         
            // if form is valid
            if(data.message){
              msgCall('success',data.message,'#actionmsg','#modal-default')                                                
            }else{
              alertCall('alert','Something wrong when input data! Please Try again','#msgtxt')
              $("html, body").scrollTop(0); 
            }  
        },
        error : function(err){
          alertCall('alert',err.responseText,'#msgtxt')
          $("html, body").scrollTop(0); 
        }
      }) 
    }
  
    function deletedata(id){
        $.ajax({
          type: 'DELETE',
          url: BASE_URL+'/'+id,
          crossDomain:true,
          dataType : 'json',
          success: function(data){
            //console.log(data)
            if(data.message){
              msgCall('success',data.message,'#actionmsg','#modal-danger-delete')
            }else{
              msgCall('alert','Unsuccessfully Delete Data','#actionmsg','#modal-danger-delete')
            }
          }
        })
    }

    function addnewpkg(idbook){
      $.redirect(base+'admin/package/action', {'idbook': idbook});
    }

    function printpdf(id){
      $.redirect(base+'admin/package/report', {'idbook': id});
    }
    
    // modal delete action
  $('#modal-danger-delete').on('show.bs.modal', function(e) {
    var id = $(e.relatedTarget).data('id');
    $('#yesbtn').on('click',function(){
        deletedata(id)
    })
    });
    // function helper
    $('#search-text').keyup(function(){
        var search = $(this).val();
    if(search != "")
    {
     getData(search)
    }
    else
    {
      getData();
    } 
    });  

    function alertCall($type,$msg,$target){
      if($type == 'alert'){
        alt= '<div class="alert alert-danger alert-dismissible">'+
                      '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
                      '<h5><i class="icon fas fa-ban"></i> Alert!</h5>'+$msg+
                    '</div>'
      }else if($type == 'success'){
        alt= '<div class="alert alert-success alert-dismissible">'+
                      '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
                      '<h5><i class="icon fas fa-check"></i> Success!</h5>'+$msg+
                    '</div>'
      }
      $($target).html(alt)
    }

    function msgCall(type,msg,target,modaltarget){
      if(type == 'alert'){
        ret= ' <div class="card card-danger"><div class="card-header"><h4 class="card-title">'+msg+'</h4></div></div>'
      }else if(type == 'success'){
        ret= ' <div class="card card-success"><div class="card-header"><h4 class="card-title">'+msg+'</h4></div></div>'
      }
      $(target).html(ret)
      getData()
      $(modaltarget).modal('hide');
    }

    function setOffSelect(target,action){
        if(action){
            $(target).attr('readonly',true);
            $(target).attr('style',"pointer-events: none;background: #eee;");
        }else{
            $(target).attr('readonly',false);
            $(target).attr('style',"pointer-events: initial;background: #ffffff;");
        }
            
    }
    
    