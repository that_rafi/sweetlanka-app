var pathname = window.location.pathname.split('/');

    var BASE_URL = 'https://sweetlanka-server.herokuapp.com/api/packagetour'
    const params = new URLSearchParams(window.location.search);
    // https://sweetlanka-server.herokuapp.com/api/packagetour
     // http://localhost:3000/api/packagetour
    var dataPackagetour = []

    function gotoactionpage(){
        window.location = '/admin/package/action'
    }
    // manage Package 
      getData()
      function getData(query=""){ 
        var keyword= params.get("search");
        if(query!= ""){
          keyword = query
        }
        var page = params.get("page");
        var limit = params.get("limit");
          $.ajax({
              type:'GET',
              url: BASE_URL+'?search='+keyword+'&page='+page+'&limit='+limit,
              dataType: 'json',
              success: function(data){
                  console.log(data);
                  var row = '';var loca_row='';
                  // table
                  for(var i=0;i<data.data.length;i++){
                      dataPackagetour[i] = {
                        _id : data.data[i]._id,
                        name : data.data[i].name,
                        desc : data.data[i].desc,
                        facility : data.data[i].facility,
                        base_price : data.data[i].base_price,
                        pickup_location : data.data[i].pickup_location,
                        tags : data.data[i].tags,
                        itinerary : data.data[i].itinerary,
                        status : data.data[i].status
                      }
                      row += '<tr>'+
                        '<td>'+data.data[i]._id+'</td>'+
                        '<td>'+data.data[i].name+'</td>'+
                        '<td>'+data.data[i].desc.slice(0, 20) + (data.data[i].desc.length > 20 ? "..." : "")+'</td>'+
                        '<td>'+data.data[i].tags.slice(0, 20)+ (data.data[i].tags.length > 20 ? "..." : "") +'</td>'+
                        '<td>'+data.data[i].base_price+'</td>'+
                        '<td>'+data.data[i].status+'</td>'+
                        '<td>'+'<a href="/admin/package/action?id='+data.data[i]._id+'"><button type="button" data-toggle="modal"  data-target="#modal-default" style="margin-right:5px" class="btn btn-outline-primary"><i class="fas fa-edit"></i></button></a>'+
                        '<button type="button" data-toggle="modal" data-target="#modal-danger-delete" data-id="'+data.data[i]._id+'" id="btndel" class="btn btn-outline-danger"><i class="fas fa-trash"></i></button>'+
                        '</td>'+
                      '</tr>'
                  }
                  // Pagination
                  var pageNumber = (Number(data.current) > 5 ? Number(data.current) - 4 : 1)
                  var pageRow = '';
                  if(data.current == 1){
                    pageRow += '<li class="page-item disabled" id="pageFirst"><a class="page-link">First</a></li>';
                  }else{
                    pageRow += '<li class="page-item" id="pageFirst"><a class="page-link" href="'+window.location.origin+'/admin/packagetour?page=1">First</a></li>';
                  }
                  if (pageNumber !== 1) {
                    pageRow += '<li class="page-item disabled" id="pageDotf"><a class="page-link">...</a></li>';
                  }
                  for (; pageNumber <= (Number(data.current) + 4) && pageNumber <= data.pages; pageNumber++) {
                    if (pageNumber == data.current) {
                      pageRow += '<li class="page-item active" ><a class="page-link" href="'+window.location.origin+'/admin/packagetour?page='+pageNumber+'">'+pageNumber+'</a></li>';
                    }else{
                      pageRow += '<li class="page-item" ><a class="page-link" href="'+window.location.origin+'/admin/packagetour?page='+pageNumber+'">'+pageNumber+'</a></li>';
                    }
                    if (pageNumber == Number(data.current) + 4 && pageNumber < data.pages) {
                     pageRow += '<li class="page-item disabled" id="pageDotl"><a class="page-link">...</a></li>';
                    }
                  }
                  
                  if (data.current == data.pages) {
                    pageRow += '<li class="page-item disabled" id="pageLast"><a class="page-link">Last</a></li>';
                  }else{
                    pageRow += '<li class="page-item" id="pageLast"><a class="page-link" href="'+window.location.origin+'/admin/packagetour?page='+data.pages+'">Last</a></li>';
                  }
                  $("#pageItem").html(pageRow)
                  // // form 
                  // for (var loc of data.location){
                  //   loca_row += '<option value="'+loc.id_location+'">'+loc.location_name+'</option>'
                  // }
                  $('#data-package').html(row) 
                  // $('#inputtype').html(type_row)
                  // $('#inputlocation').html(loca_row)
                  if(query!=""){var html = '<div></div>'}else{var html = '<div></div>'}
                  $('#page').html(html)
              }
          })
          
      }

    
    // modal delete action
  $('#modal-danger-delete').on('show.bs.modal', function(e) {
    var id = $(e.relatedTarget).data('id');
    $('#yesbtn').on('click',function(){
        deletedata(id)
    })
    });

    function deletedata(id){
      $.ajax({
        type: 'DELETE',
        url: BASE_URL+'/'+id,
        crossDomain:true,
        dataType : 'json',
        success: function(data){
          //console.log(data)
          if(data.message){
            msgCall('success',data.message,'#actionmsg','#modal-danger-delete')
          }else{
            msgCall('alert','Unsuccessfully Delete Data','#actionmsg','#modal-danger-delete')
          }
        }
      })
  }
    // function helper
    $('#search-text').keyup(function(){
        var search = $(this).val();
    if(search != "")
    {
     getData(search)
    }
    else
    {
      getData();
    } 
    });  

    function msgCall(type,msg,target,modaltarget){
      if(type == 'alert'){
        ret= ' <div class="card card-danger"><div class="card-header"><h4 class="card-title">'+msg+'</h4></div></div>'
      }else if(type == 'success'){
        ret= ' <div class="card card-success"><div class="card-header"><h4 class="card-title">'+msg+'</h4></div></div>'
      }
      $(target).html(ret)
      getData()
      $(modaltarget).modal('hide');
    }