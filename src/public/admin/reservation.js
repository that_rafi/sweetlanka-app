var BASE_URL = 'https://sweetlanka-server.herokuapp.com/api/reservation'
var BASE_RECOMMENDATION_URL = 'https://sweetlanka-server.herokuapp.com/api/recommendation'
const params = new URLSearchParams(window.location.search);
var recommendationId = ""
var rsvId = params.get('rsvId')
    // https://sweetlanka-server.herokuapp.com/api/packagetour
     // http://localhost:3000/api/packagetour

     $(function() {      
        var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;      
        $(window).on('resize',function(){
            var widthresize = (window.innerWidth > 0) ? window.innerWidth : screen.width;
            if(widthresize <= 500){
                $('#pkgresult').attr('style','max-width: 24rem;margin-top:15px;')
                $('#msgresult').attr('style','max-width: 24rem;margin-top:15px;')
                $('#msgtxt').attr('style','max-width: 450px;')
            }else{
                $('#pkgresult').attr('style','max-width: 32rem;margin-top:15px;')
                $('#msgresult').attr('style','max-width: 32rem;margin-top:15px;')
                $('#msgtxt').attr('style','')
            }
        })
        if(width<=500){
            $('#pkgresult').attr('style','max-width: 24rem;margin-top:15px;')
            $('#msgresult').attr('style','max-width: 24rem;margin-top:15px;')
            $('#msgtxt').attr('style','max-width: 450px;')
        }else{
            $('#pkgresult').attr('style','max-width: 32rem;margin-top:15px;')
            $('#msgresult').attr('style','max-width: 32rem;margin-top:15px;')
            $('#msgtxt').attr('style','')
        }
        if(rsvId!=null ){
           loading(true,'#mainform')     
            getRecommendation()
        }else{
            loading(false,'#mainform')
            $('#confirmation').attr('hidden',true);
        }
        getSelectFormData()
     })

    $('#btnyes').on("click",function(){
        
        inputdata()
        jQuery('#exampleModal').modal('hide')
        loading(true,'#mainform')
    })

    $('#btnconfirm').on("click", function(){
        updatePackage()
        loading(true,'#confirmation')
    })

    $('#btnokthank').on("click", function(){
        window.location = '/reservation'
    })
     

    function getSelectFormData(){
        $.getJSON("./assets/coutries.json", function(json) {
            var locRow = []
            for( var i in json){
                locRow += '<option value="'+json[i].name+'">'+json[i].name+'</option>'
            }
            $('#loc').html(locRow)
        });

        var durRow = []
        for (let i = 1; i < 15; i++) {
            durRow +=  '<option value="'+i+'">'+i+'</option>'
        }
        $('#dur').html(durRow)
    }

    function formatDate(initdate){
    return initdate.toLocaleDateString('en-GB', {day : 'numeric',month : 'long',year : 'numeric'}).split(' ').toString().replace(','," "); 
  }
  function incrementDate(next_day,duration){
    var dates = [];
    dates[0]= formatDate(next_day); 
    for(var i=1;i<duration;i++){
    next_day.setDate(next_day.getDate() + 1);
    dates[i]=formatDate(next_day); 
    }
    return dates;
  }

  function inputdata() {
      var data = $('#datareservation').serializeArray();
      data.push({name : 'packageId', value : ""})
      data.push({name : 'status', value : 'Pending'})
      $.ajax({
        type: 'POST',
        url: BASE_URL,
        data: data,
        crossDomain:true,
        dataType: 'json',
        success: function(data){
            console.log(data);     
            // if form is valid
            if(data.message){
                if(data._id){
                    $("html, body").scrollTop(0); 
                    window.location = '/reservation?rsvId='+data._id
                }else{
                    modalCall('alert',data.message)
                    loading(false,'#mainform')
                }                                       
            }else{
                modalCall('alert','Something wrong when input data! Please Try again')
            }  
         },
        error : function(err){
          modalCall('alert',err.responseText)
          $("html, body").scrollTop(0);
          loading(false,'#mainform')
        }
      })
  }

  function updatePackage(){
        $.ajax({
            type: 'PATCH',
            url: BASE_URL+'/'+rsvId,
            data: 'packageId='+recommendationId+'&updatePkg='+true,
            crossDomain:true,
            dataType: 'json',
            success: function(data){
                //console.log(data);     
                // if form is valid
                if(data.message){
                    $("html, body").scrollTop(0); 
                    loading(false,'#confirmation')
                    modalThank('Thank you for the reservation, we will reach you out!')              
                }else{
                    modalCall('alert','Something wrong when input data! Please Try again')
                }  
            },
            error : function(err){
            modalCall('alert',err.responseText)
            $("html, body").scrollTop(0);
            loading(false,'#confirmation')
            }
        })
  }

  function getRecommendation(){  
      $.ajax({
          type : 'POST',
          url : BASE_RECOMMENDATION_URL,
          data : 'reservationId='+params.get('rsvId'),
          dataType : 'json',
          success: function(data){
            console.log(data);
            loading(false,'#confirmation') 
                var tags_row= '';var tagspkg_row='';
                var rsvUser = data.reservation;
                var recommendation = data.recommendation
                recommendationId = recommendation._id
				// if(data.result_in_duration!=""){ // if based on dur is exists
				// 	res = data.result_in_duration[0];
				// 	$('#msgresult').attr('hidden',true);
				// }else{ // if on non dur		
				// 	res = data.result_in_nonduration[0];
				// 	$('#msgresult').attr('hidden',false);
				// 	$('#msgresult').text('Currently your request is not available for '+data.bookdata.duration+' Days trips, but we recommend you to check our alternative offers! Thanks you very much');
				// }
			    // tags pref
                if(rsvUser.packageId && rsvUser.status == "Pending"){
                    modalThank('You are already reserved, Please wait for a moment, We will reach you out!')
                }
                var itinerary = recommendation.itinerary
                var userPref = rsvUser.preferences.split(',');
                var recomPref = recommendation.tags.split(',')
				for(var i=0;i<userPref.length;i++){
					tags_row+= '<font style="margin-right:5px" class="badge badge-primary">'+userPref[i]+'</font>'
				}
				for(var i=0;i<recomPref.length;i++){
					tagspkg_row+= '<font style="margin-right:5px" class="badge badge-primary">'+recomPref[i]+'</font>'
				}
				$('#userpref').html(tags_row)
				// title & DESC , tags
				$('#pkgtitle').text(recommendation.name)
				$('#pkgdesc').text(recommendation.desc)
				$('#pkgtags').html(tagspkg_row)
				// pkg image
                var imageFiles = itinerary[0].destinationId.images[0].image
                var fileNameIndex = imageFiles.lastIndexOf("/") + 1;
                var filename = imageFiles.substr(fileNameIndex);
				var	source = '../assets/upload/destination/'+filename;
				// console.log(source)
				$('#pkgimg').attr('src',source)
				// itinerary
				var itin_row='';
				var next_date = new Date(rsvUser.start_date);
				var date = incrementDate(next_date,itinerary.length)
				for(var i in itinerary ){
					itin_row += '<div id="itindateday"><h4><i class="icon fas fa-clock"></i>Day '+(parseInt(i)+1)+' ( '+date[i]+' ) - '+itinerary[i].title+'</h4></div>'+
                '<div><label id="destname"><i class="icon fas fa-map-marker-alt"></i> '+itinerary[i].destinationId.name+' </label></div>'+
                '<div><h6 id="roomname"><i class="icon fas fa-home"></i> Hotel Star '+recommendation.hotel_star+' </h6></div>'+
                // '<div><h6 id="mealname"><i class="icon fas fa-utensils"></i> '+itin.meal_name+' </h6></div>'+
                '<div id="itinprogram"><p>'+itinerary[i].program+'</p></div>'
           		 }    
				$('#totalcost').html("<h4>Total Cost : $"+recommendation.base_price+"</h4></div>")
                $('#pkgitin').html(itin_row)
			// 	// vehicle info
			// 	var vehi_row="";
			// 	if(res.package.id_transport!=""){
			// 		vehi_row += '<li>Name : '+res.package.transport_name+'</li>'+
			// 		'<li>Model : '+res.package.model+'</li>'+
			// 		'<li>Air Conditioner : '+res.package.air_cond+'</li>'+
			// 		'<li>Seat : '+res.package.seat+'</li>'+
			// 		'<li>Lugage Space : '+res.package.lugage_space+'</li>'
			// 	}
			// 	$('#vinfo').html(vehi_row)
			// 	// include exclude
			// 	var inc_row="";var ex_row=""
			// 	for(var inc of res.include){
            //     inc_row+= '<li>'+inc.facility_name+'</li>'
			// 	}
			// 	for(var ex of res.exclude){
			// 		ex_row+= '<li>'+ex.facility_name+'</li>'
			// 	}
			// 	$('#include').html(inc_row)
			// 	$('#exclude').html(ex_row)
			// 	// SEND SOCIAL MEDIA
			// 	var mssg = 'Hello Sweetlanka ! This is my reservation ref RSV'+data.bookdata.id_booking;
			// 	var phone = '6289607888340';
			// 	$('#sendwa').attr('href','https://wa.me/'+phone+'?text='+encodeURI(mssg))
			// 	// btn confirmation
			// 	$('#btnconfirm').on('click',function(){
			// 		updateBooking(data.bookdata.id_booking,res.package.id_package)
			// 		loading(true,'#confirmation')
			// 	})
			// 	console.log(res)
          },
          error : function(err){
            console.log(err);
            window.location = '/reservation'
            $("html, body").scrollTop(0);
          }
      })
  }

  function modalCall(type,msg){
    if(type!="alert"){
      $('#modaltype').attr('class', 'modal-content bg-success');
    }else{
      $('#modaltype').attr('class', 'modal-content bg-danger');
    }
    $('#modaltitle').text(type.toUpperCase())
    var mssg = '<font color="white">'+msg+'</font>'
    $('#modalmsg').html(mssg)
    jQuery('#modal-info').modal('show')
  }

  function formatDate(initdate){
    return initdate.toLocaleDateString('en-GB', {day : 'numeric',month : 'long',year : 'numeric'}).split(' ').toString().replace(','," "); 
  }
  function incrementDate(next_day,duration){
    var dates = [];
    dates[0]= formatDate(next_day); 
    for(var i=1;i<duration;i++){
    next_day.setDate(next_day.getDate() + 1);
    dates[i]=formatDate(next_day); 
    }
    return dates;
  }

  function modalThank(text){
    jQuery('#modalThank').modal('show')
    $('#modalThanktxt').text(text)
  }

function loading(action,target){
    if(action){
        $('#loading').attr('hidden',false);
        $(target).attr('hidden',true);
    }else{
        $('#loading').attr('hidden',true);
        $(target).attr('hidden',false);
    }
}

