const express = require('express');
const router = express.Router();
const path = require('path');
const verifyAccess = require('./verifyToken')

const formidable = require('express-formidable')
const opts = {
  encoding : 'utf-8',
  keepExtensions : true,
  multiples: true,
  maxFileSize : 5 * 1042 * 1042,
  isOK : true
}
const opts2 = {
  encoding : 'utf-8',
  multiples: true,
  isOK : true
}
const saveTo = (target) => [
  {
    event : 'fileBegin',
    action : function(req,res,next,name,file){
      const fileType = file.type.split('/').pop();
      const fileName = Date.now()+'_'+file.name.split('.').shift();
      if(file.type.startsWith('image/')){
        file.path = target+fileName+'.'+fileType
      }else{
        console.log('File type is not supported');
        opts.isOK = false
      }
    }
  }
]

import homepageController from "../controller/homepageController";
import aboutController from "../controller/aboutController";
import blogController from "../controller/blogController";
import destinationController from "../controller/destinationController";
import reservationController from "../controller/reservationController";
import chatbotController from "../controller/chatbotController";

// admin

import loginController from "../controller/Admin/loginController";
import dashboardController from "../controller/Admin/dashboardController";
import manageDestination from "../controller/Admin/destinationController";
import managePackage from "../controller/Admin/packageController";
import manageReservation from "../controller/Admin/reservationController";
import manageArticle from "../controller/Admin/articleController";
import manageIntent from "../controller/Admin/intentController";


let initWebRoutes = (app) => {
    // customer
    router.get("/",homepageController.getHomepage)
    router.get("/about", aboutController.getAboutpage)
    router.get("/blog",blogController.getBlogpage)
    router.get("/blog/detail",blogController.getBlogDetailpage)
    router.get("/destination",destinationController.getDestinationpage)
    router.get("/destination/detail",destinationController.getDestinationDetailpage)
    router.get("/reservation",reservationController.getReservationpage)
    // admin
    router.get("/admin",verifyAccess,dashboardController.getDashboardpage)
    router.get("/admin/login",loginController.getLoginpage)
    router.get("/admin/logout",loginController.logout)
    router.get("/admin/destination",verifyAccess,manageDestination.getDestinationpage)
    router.post("/admin/destination/upload" , formidable(opts,saveTo('./src/public/assets/upload/destination/')) , manageDestination.uploadDestinationImg)
    router.post("/admin/destination/delete" , formidable(opts2), manageDestination.deleteDestinationImage)
    router.get("/admin/package",verifyAccess, managePackage.getPackagepage)
    router.get("/admin/package/action",verifyAccess, managePackage.getPackageactionpage )
    router.get("/admin/reservation",verifyAccess,manageReservation.getReservationpage)
    router.get("/admin/article",verifyAccess,manageArticle.getArticlepage)
    router.post("/admin/article/upload" , formidable(opts,saveTo('./src/public/assets/upload/article/')) , manageArticle.uploadArticleImg)
    router.post("/admin/article/delete" , formidable(opts2), manageArticle.deleteArticleImage)
    router.get("/admin/intent",verifyAccess, manageIntent.getIntentpage)
    // facebook
    router.get("/webhook", chatbotController.getWebhook)
    router.post("/webhook",chatbotController.postWebhook)
    router.post("/set-up-user-fb-profile", homepageController.setUpFacebookProfile)

    return app.use("/",router);
}




module.exports = initWebRoutes;