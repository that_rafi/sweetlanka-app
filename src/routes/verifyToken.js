const jwt = require('jsonwebtoken')

module.exports = function (req,res,next){
  const token = req.cookies.key
  if(!token) return res.redirect('login')

  try{
    jwt.verify(token,process.env.TOKEN_SECRET)
    // req.user = verified // reveal data stored in jwt
    // next(); // important
    next()
  }catch(err){
    console.log('Invalid Token');
    res.redirect('login')
  }

}
