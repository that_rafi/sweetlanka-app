require('dotenv').config()
import request from 'request'
var PAGE_ACCESS_TOKEN = process.env.PAGE_ACCESS_TOKEN

const getHomepage = async (req,res) =>{
  request({
    "uri": 'https://sweetlanka-server.herokuapp.com/api/destination',
    "method": "GET"
  }, (err,response, body) => {
    if (!err) {
      getArticle(function (data) {
        var dest =  JSON.parse(body)
        // console.log(dest);
        //return res.render('homepage', {destination : body })
        return res.render("homepage", {file: dest, article: data}, function(e, dt) {
          // Send the compiled HTML as the response
          res.send(dt.toString());
        });
      })    
    } else {
      console.error("Unable to send message:" + err);
      req.end()
    }
  });
    
};

function getArticle(callback) {
  request({
    "uri": 'https://sweetlanka-server.herokuapp.com/api/article',
    "method": "GET"
  }, (err,request, body) => {
    if (!err) {
      var dest =  JSON.parse(body)
      callback(dest)
    } else {
      console.error("Unable to send message:" + err);
      request.end()
    }
  });
}

let setUpFacebookProfile = (req,res) => {
// Send the HTTP request to the Messenger Platform
let data = {
  "get_started":{
    "payload":"GET_STARTED"
  },
  "persistent_menu": [
    {
        "locale": "default",
        "composer_input_disabled": false,
        "call_to_actions": [
            {
                "type": "postback",
                "title": "Talk to an agent",
                "payload": "CARE_HELP"
            },
            {
              "type": "postback",
              "title": "Talk with admin",
              "payload": "TALK_ADMIN"
            },
            {
              "type": "web_url",
              "title": "Reservation now",
              "url": "https://sweetlanka-app.herokuapp.com/reservation",
              "webview_height_ratio": "full"
          }
        ]
    }
 ]
}
let dataDelete = {
  "fields": [
    "get_started",
    "persistent_menu",
  ]
}
  
  
request({
    "uri": `https://graph.facebook.com/v9.0/me/messenger_profile?access_token=${PAGE_ACCESS_TOKEN}`,
    "method": "POST",
    "json": data
  }, (err, body) => {
    if (!err) {
      return res.status(200).json({ 
        message : "Setup OK"
      })
    } else {
      return res.status(500).json({
          "message" : "error from the node server"
      })
    }
  });
}

module.exports = {
    getHomepage : getHomepage,
    setUpFacebookProfile : setUpFacebookProfile
}