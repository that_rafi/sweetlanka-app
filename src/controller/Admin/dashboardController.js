const getDashboardpage = (req,res) => {
    res.render('admin/dashboard');
}

module.exports = {
    getDashboardpage : getDashboardpage
}