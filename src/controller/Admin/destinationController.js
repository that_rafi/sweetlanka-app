const fs = require('fs')
 const getDestinationpage = (req,res) =>{
    res.render('admin/manage_destination')
}



const uploadDestinationImg = (req,res) =>{
        var imageFiles = []
        var detailsData = req.fields.details.split(',')
        var fileKey = Object.keys(req.files)[0];
        if(req.fields.imageCount == 1){
            imageFiles.push({
                image : req.files[fileKey].path,
                detail : req.fields.details
            })
        }else{
            for ( var i in req.files[fileKey] ){
                imageFiles.push({ 
                        image : req.files[fileKey][i].path,
                         detail : detailsData[i]
                    });
            }
        }
        
        res.status(200).send({
            result : imageFiles, 
            count : req.fields.imageCount });
    
}

const deleteDestinationImage = (req,res) =>{
    var dataImages = JSON.parse(req.fields.images);
    var images = []
    for(var i in dataImages){
        images.push(dataImages[i].image)
    }
    //console.log(images);
    try {
        images.forEach(path => fs.existsSync(path) && fs.unlinkSync(path))
        res.status(200).send({message : 'success delete file'})
      } catch (err) {
        // error handling here
        console.error(err)
      }
    
}

module.exports = {
    getDestinationpage : getDestinationpage,
    uploadDestinationImg : uploadDestinationImg,
    deleteDestinationImage : deleteDestinationImage
}