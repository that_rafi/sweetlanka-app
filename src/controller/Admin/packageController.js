const getPackagepage = (req,res) =>{
    res.render('admin/manage_package')
}

const getPackageactionpage = (req,res) => {
    res.render('admin/manage_package_action')
}

module.exports = {
    getPackagepage : getPackagepage,
    getPackageactionpage : getPackageactionpage
}