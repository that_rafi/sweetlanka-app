const getLoginpage = (req,res) => {
    res.render('admin/login')
}

const logout = (req,res) => {
    res.cookie("key","");
    res.cookie("email","");
    res.redirect('login')
}

module.exports = {
    getLoginpage : getLoginpage,
    logout : logout
}