require('dotenv').config()
const request = require('request');
import chatbotService from "../service/chatbotService"
const MY_VERIFY_TOKEN = process.env.MY_VERIFY_TOKEN
const PAGE_ACCESS_TOKEN = process.env.PAGE_ACCESS_TOKEN
var isChatbot = false;

const postWebhook = (req,res)=> {
   // Parse the request body from the POST
  let body = req.body;

  // Check the webhook event is from a Page subscription
  if (body.object === 'page') {

    // Iterate over each entry - there may be multiple if batched
    body.entry.forEach(function(entry) {

        // Gets the body of the webhook event
    let webhook_event = entry.messaging[0];
    console.log(webhook_event);


    // Get the sender PSID
    let sender_psid = webhook_event.sender.id;
    console.log('Sender PSID: ' + sender_psid);

    // Check if the event is a message or postback and
    // pass the event to the appropriate handler function
    if (webhook_event.message) {
        handleMessage(sender_psid, webhook_event.message);        
    } else if (webhook_event.postback) {
        handlePostback(sender_psid, webhook_event.postback);
    }
      
    });

    // Return a '200 OK' response to all events
    res.status(200).send('EVENT_RECEIVED');

  } else {
    // Return a '404 Not Found' if event is not from a page subscription
    res.sendStatus(404);
  }
}

const getWebhook = (req,res) => {
    let VERIFY_TOKEN = MY_VERIFY_TOKEN
    
  // Parse the query params
  let mode = req.query['hub.mode'];
  let token = req.query['hub.verify_token'];
  let challenge = req.query['hub.challenge'];
    
  // Checks if a token and mode is in the query string of the request
  if (mode && token) {
  
    // Checks the mode and token sent is correct
    if (mode === 'subscribe' && token === VERIFY_TOKEN) {
      
      // Responds with the challenge token from the request
      console.log('WEBHOOK_VERIFIED');
      res.status(200).send(challenge);
    
    } else {
      // Responds with '403 Forbidden' if verify tokens do not match
      res.sendStatus(403);      
    }
  }
}

// Handles messages events
const handleMessage = async (sender_psid, received_message)  => {
    let response;

  // Check if the message contains text
  if (received_message.text) {    

    // Create the payload for a basic text message
    if(isChatbot){
      let chatResponse = await chatbotService.getResponseChatbot(received_message.text)
      response = {"text" : `${chatResponse}`}
    }
    
  }  
  
  // Sends the response message
  callSendAPI(sender_psid, response); 
}

// Handles messaging_postbacks events
const handlePostback = async (sender_psid, received_postback) => {
  let response;
  
  // Get the payload for the postback
  let payload = received_postback.payload;

  switch(payload){
    case "GET_STARTED": 
    response = {"text" : "Hello welcome to sweetlanka"}
    break;
    case "CARE_HELP" :
      let username = await chatbotService.getUsername(sender_psid)
      response = {"text" : `You are talking with agent, how can i help you ${username}`}
      isChatbot = true;
    break;
    case "TALK_ADMIN" : 
      response = {"text" : `You are talking with admin now, Please wait until the admin online. Thank you`}
      isChatbot = false
    break;
  }
  // Send the message to acknowledge the postback
  callSendAPI(sender_psid, response);
}

// Sends response messages via the Send API
function callSendAPI(sender_psid, response) {
  // Construct the message body
  let request_body = {
    "recipient": {
      "id": sender_psid
    },
    "message": response
  }

  // Send the HTTP request to the Messenger Platform
  request({
    "uri": "https://graph.facebook.com/v2.6/me/messages",
    "qs": { "access_token":  PAGE_ACCESS_TOKEN},
    "method": "POST",
    "json": request_body
  }, (err, res, body) => {
    if (!err) {
      console.log('message sent!')
    } else {
      console.error("Unable to send message:" + err);
    }
  });
}




module.exports = {
    postWebhook : postWebhook,
    getWebhook : getWebhook
}