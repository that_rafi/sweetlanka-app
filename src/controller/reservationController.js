const getReservationpage = (req,res) => {
    return res.render('reservation')
}

module.exports = {
    getReservationpage : getReservationpage
}