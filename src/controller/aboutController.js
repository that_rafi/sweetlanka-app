const getAboutpage = (req,res) => {
    return res.render('about');
}

module.exports = {
    getAboutpage : getAboutpage
}