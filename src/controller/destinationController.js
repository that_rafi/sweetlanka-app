const getDestinationpage = (req,res) => {
     return res.render('destination')
}

const getDestinationDetailpage = (req,res) => {
    return res.render('destination_detail')
}

module.exports = {
    getDestinationpage : getDestinationpage,
    getDestinationDetailpage : getDestinationDetailpage
}