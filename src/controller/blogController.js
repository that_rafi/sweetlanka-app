const getBlogpage = (req,res) => {
    return res.render("blog");
}

const getBlogDetailpage = (req,res) => {
    return res.render("blog_detail");
}

module.exports = {
    getBlogpage : getBlogpage,
    getBlogDetailpage : getBlogDetailpage
}