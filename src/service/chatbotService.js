import request from "request";
require('dotenv').config
var PAGE_ACCESS_TOKEN = process.env.PAGE_ACCESS_TOKEN

function getUsername(sender_psid) {
    return new Promise ((resolve,reject)=>{
        request({
            "uri": `https://graph.facebook.com/${sender_psid}?fields=first_name,last_name,profile_pic&access_token=${PAGE_ACCESS_TOKEN}`,
            "method": "GET",
          }, (err, res, body) => {
            if (!err) {
                body = JSON.parse(body)
                let uname = `${body.last_name} ${body.first_name}`
                resolve(uname)       
            } else {
              reject("Unable to send message:" + err);
            }
          });
    })
    
}

function getResponseChatbot(qst) {
  return new Promise ((resolve,reject)=>{
    request({
        "uri": `https://sweetlanka-server.herokuapp.com/api/test`,
        "method": "POST",
        "json" : {question : qst }
      }, (err, res, body) => {
        if (!err) {
          console.log(body);
            let response = `${body.response}`
            resolve(response) 
            console.log(response);      
        } else {
          reject("Unable to send message:" + err);
        }
      });
})
}

module.exports = {
    getUsername : getUsername,
    getResponseChatbot : getResponseChatbot
}